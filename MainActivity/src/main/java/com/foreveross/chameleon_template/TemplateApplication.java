package com.foreveross.chameleon_template;

import com.foreveross.chameleonsdk.CApplication;
import com.foreveross.chameleonsdk.CModule;
import com.foss.ChameleonApplication;
import org.acra.CrashReport;

import java.io.InputStream;
import java.util.HashMap;

public class TemplateApplication extends ChameleonApplication {

    public CApplication cApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        getCApplication();
        initModule();
        CrashReport crashReport = new CrashReport();
        crashReport.start(this);
    }

    public CApplication getCApplication() {
        if(cApplication == null) {
            InputStream is = getFromAssets("bsl.json");
            cApplication = new CApplication(this, is);
        }
        return cApplication;
    }

    private void initModule() {
        HashMap<String, CModule> modules = cApplication.getModules();
        for(CModule module : modules.values()) {
            module.onCreate(module);
        }
    }


    public InputStream getFromAssets(String fileName) {
        InputStream is = null;
        try {
            is = this.getResources().getAssets().open(fileName);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return is;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void exitApp() {
        HashMap<String, CModule> modules = cApplication.getModules();
        for(CModule module : modules.values()) {
            module.onExit(module);
        }
    }
}
