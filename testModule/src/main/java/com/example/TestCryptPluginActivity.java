package com.example;

import android.os.Bundle;
import org.apache.cordova.CordovaActivity;

public class TestCryptPluginActivity extends CordovaActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();

        loadUrl("file:///android_asset/crypt.html");
    }
}
