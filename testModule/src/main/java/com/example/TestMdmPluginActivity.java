package com.example;

import android.os.Bundle;
import org.apache.cordova.CordovaActivity;

/**
 * @author wzq
 * @description mdm 测试页
 */
public class TestMdmPluginActivity extends CordovaActivity {

    public final static String TAG = "TestPushPluginActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();
        loadUrl("file:///android_asset/mdm.html");
    }
}
