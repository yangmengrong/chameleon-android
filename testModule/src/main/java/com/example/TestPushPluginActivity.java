package com.example;

import android.os.Bundle;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaPlugin;

/**
 * Created by zhouzhineng on 14-7-29.
 */
public class TestPushPluginActivity extends CordovaActivity {

    public final static String TAG = "TestPushPluginActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();
        loadUrl("file:///android_asset/push.html");
    }
}
