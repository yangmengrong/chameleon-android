package com.example.sqlcrypt;

/**
 * description: 数据模型
 *
 * @author: wzq
 */
public class BeanStu {
    private String name;
    private String addr;
    private int age;

    public BeanStu() {
        super();
    }

    public BeanStu(String aName, String aAddr, int aAge) {
        super();
        name = aName;
        addr = aAddr;
        age = aAge;
    }

    public String getName() {
        return name;
    }

    public void setName(String aName) {
        name = aName;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String aAddr) {
        addr = aAddr;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int aAge) {
        age = aAge;
    }

    @Override
    public String toString() {
        return "BeanStu [name=" + name + ", addr=" + addr + ", age=" + age + "]";
    }

    public static BeanStu genRdm() {
        String name = "nameAAA";
        String addr = "addrGz";
        int age = 100;
        return new BeanStu(name, addr, age);
    }
}
