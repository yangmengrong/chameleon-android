package com.foreveross.bsl.util;

import com.foreveross.chameleonsdk.config.URL;

public class BroadcastConstans {
	public static final String PACKAGENAME = URL.APP_PACKAGENAME;
	public static final String identifier = PACKAGENAME+"com.foreveross.chameleon";
	public final static String MODULE_AUTODOWNLOAD_PROGERSS =PACKAGENAME+"module.autodownload.progerss";
	public final static String MODULE_AUTODOWNLOAD_START =PACKAGENAME+"module.autodownload.progerss.start";
	public final static String MODULE_AUTODOWNLOAD_FINISH =PACKAGENAME+"module.autodownload.progerss.finish";
	public static final String APP_UPDATE =PACKAGENAME+ "com.app.update";
	public static final String RefreshMainPage =PACKAGENAME+"com.foss.refreshMainPage";
	public static final String RefreshModule = PACKAGENAME+"com.foss.refreshModule";
	public static final String ReceiveMessage = PACKAGENAME + "com.foss.receiveMessage";
	public static final String UpdateProgress= PACKAGENAME+ "com.foss.updateProgress";
	public static final String SecurityChange = PACKAGENAME + "com.foss.SecurityChange";
	public static final String SecurityRefreshMainPage =PACKAGENAME + "com.foss.SecurityRefreshMainPage";
	public static final String SecurityRefreshModuelDetail =PACKAGENAME + "com.foss.SecurityRefreshModuelDetail";
	public static final String ReceiveMessages =PACKAGENAME + "com.foss.receiveMessages";
	public static final String SecurityRoleChange = PACKAGENAME + "com.foss.SecurityRoleChange";
	public static final String SecurityChangeForFile = PACKAGENAME + "com.foss.SecurityChangeForFile";

	public static final String MODULE_CHANGE = PACKAGENAME+"com.foss.cubeModelChange";
	public static final String MODULE_RESET = PACKAGENAME+"com.foss.reset";
	public static final String MODULE_WEB = PACKAGENAME+"com.foss.module.web";
	public final static String MODULE_PROGRESS =PACKAGENAME+"com.foreveross.module_process";
	public static final String PUSH_CHAT= PACKAGENAME+"push.user.chat";
	public static final String PUSH_MutipleAccount =PACKAGENAME+ "com.xmpp.mutipleAccount";
	public static final String  KEYBOARDSHOW = PACKAGENAME+"com.foss.isKeyboardShow";
}
