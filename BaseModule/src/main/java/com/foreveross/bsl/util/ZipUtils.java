package com.foreveross.bsl.util;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipUtils {
    /**
     * 解压文件
     *
     * @param zipFile
     * @param targetDir
     *
     * @throws Exception
     */
    public static boolean unZipFile(String zipFile, String targetDir) throws Exception {
        File archive = new File(zipFile);
        try {
            ZipFile zipfile = new ZipFile(archive);
            for(Enumeration e = zipfile.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                unzipEntry(zipfile, entry, targetDir);
            }
            return true;
        } catch(Exception e) {
        }
        return false;
    }

    private static void unzipEntry(ZipFile zipfile, ZipEntry entry, String outputDir) throws IOException {

        if(entry.isDirectory()) {
            createDir(new File(outputDir, entry.getName()));
            return;
        }

        File outputFile = new File(outputDir, entry.getName());
        if(!outputFile.getParentFile().exists()) {
            createDir(outputFile.getParentFile());
        }

        BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

        try {
            IOUtils.copy(inputStream, outputStream);
        } finally {
            outputStream.close();
            inputStream.close();
        }
    }

    private static void createDir(File dir) {
        if(dir.exists()) {
            return;
        }
        if(!dir.mkdirs()) {
            throw new RuntimeException("Can not create dir " + dir);
        }
    }
}
