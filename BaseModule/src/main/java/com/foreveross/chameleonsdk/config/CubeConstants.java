package com.foreveross.chameleonsdk.config;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import com.foreveross.chameleon.basemodule.R;
import com.foss.ChameleonApplication;


/**
 * @author fengweili</br> 2011-8-13
 */
public class CubeConstants {

    public final static int CUBE_CONFIG = R.raw.cube;
    public final static boolean DEMO = false;
    public static final int LOGIN_SUCCESS = 11;
    public static String SD_ROOT = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static String APP_PACKAGE_NAME = ChameleonApplication.getApplication().getPackageName();
    public static int APP_VERSION_CODE = getAppVer();
    public static String APP_STORAGE_DIR_SD = SD_ROOT + "/" + APP_PACKAGE_NAME;

    private static int getAppVer() {
        Context context = ChameleonApplication.getApplication();
        PackageManager pm = context.getPackageManager();
        int ver = 0;
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            ver = pi.versionCode;
        } catch(NameNotFoundException e) {
            e.printStackTrace();
        }
        return ver;
    }
}