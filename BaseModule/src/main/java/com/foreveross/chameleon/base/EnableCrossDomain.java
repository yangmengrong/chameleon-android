package com.foreveross.chameleon.base;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.Log;
import android.webkit.WebView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
/**
 * @author: wzq
 * @date: 14-6-11
 * description: change it at File | setting | File and code templates | include | file header
 */
public class EnableCrossDomain {
    private static final String TAG = EnableCrossDomain.class.getSimpleName();

    // TODO 处理跨域问题enable cross domin
    public static void enableContentAsLocal(WebView webview) {
        if(VERSION.SDK_INT <= VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            enablecrossdomain4_0(webview);
        } else {
            enablecrossdomain4_1(webview);
        }
    }

    private static void enablecrossdomain4_0(WebView webview) {
        try {
            Field field = WebView.class.getDeclaredField("mWebViewCore");
            field.setAccessible(true);
            Object webviewcore = field.get(webview);
            Method method = webviewcore.getClass().getDeclaredMethod("nativeRegisterURLSchemeAsLocal", String.class);
            method.setAccessible(true);
            method.invoke(webviewcore, "content");
            Log.i("enablecrossdomain", "success");
        } catch(Exception e) {
            Log.e(TAG, "enablecrossdomain error");
            e.printStackTrace();
            Log.e("enablecrossdomain", "fail");
        }
    }

    private static void enablecrossdomain4_1(WebView webView) {

        try {
            Method method;
            method = webView.getSettings().getClass().getMethod("setAllowUniversalAccessFromFileURLs", boolean.class);
            if(method != null) {
                method.invoke(webView.getSettings(), true);
            }
        } catch(NoSuchMethodException e) {
            Log.d(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed", e);
        } catch(IllegalArgumentException e) {
            Log.e(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed", e);
        } catch(IllegalAccessException e) {
            Log.e(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed", e);
        } catch(InvocationTargetException e) {
            Log.e(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed", e);
        }
        //
        try {
            Method method = webView.getClass().getMethod("getWebViewProvider");
            if(method != null) {
                Object provider = method.invoke(webView);
                method = provider != null ? provider.getClass().getMethod("getWebViewCore") : null;
                if(method != null) {
                    Object webViewCore = method.invoke(provider);
                    method = webViewCore != null ? webViewCore.getClass()
                            .getDeclaredMethod("nativeRegisterURLSchemeAsLocal", int.class, String.class) : null;
                    if(method != null) {
                        Field field = webViewCore.getClass().getDeclaredField("mNativeClass");
                        if(field != null) {
                            field.setAccessible(true);
                            method.setAccessible(true);
                            method.invoke(webViewCore, field.get(webViewCore), "content");
                        }
                    }
                }
            }

        } catch(NoSuchMethodException e) {
            Log.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch(IllegalArgumentException e) {
            Log.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch(IllegalAccessException e) {
            Log.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch(InvocationTargetException e) {
            Log.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch(NoSuchFieldException e) {
            Log.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        }
        //        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        //        try {
        //            Field webviewclassic_field = WebView.class.getDeclaredField("mProvider");
        //            webviewclassic_field.setAccessible(true);
        //            Object webviewclassic = webviewclassic_field.get(webView);
        //            Field webviewcore_field = webviewclassic.getClass().getDeclaredField("mWebViewCore");
        //            webviewcore_field.setAccessible(true);
        //            Object mWebViewCore = webviewcore_field.get(webviewclassic);
        //            Field nativeclass_field = webviewclassic.getClass().getDeclaredField("mNativeClass");
        //            nativeclass_field.setAccessible(true);
        //            Object mNativeClass = nativeclass_field.get(webviewclassic);
        //            Method method = mWebViewCore.getClass()
        //                    .getDeclaredMethod("nativeRegisterURLSchemeAsLocal", new Class[]{int.class, String.class});
        //            method.setAccessible(true);
        //            method.invoke(mWebViewCore, "content");
        //            Log.i("enablecrossdomain", "success");
        //        } catch(Exception e) {
        //            Log.e(TAG, "enablecrossdomain error");
        //            e.printStackTrace();
        //            Log.e("enablecrossdomain", "fail");
        //        }
    }
}
