package com.foss.mdm;
import android.app.Activity;
import android.app.Application;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.foss.AppLog;
import com.foss.ChameleonApplication;
/**
 * project:
 * author: wzq
 * date: 2014/7/31
 * description:
 */
public class MdmCmdExecutor {
    public static final int REQUEST_CODE_MDM = 0;
    private DevicePolicyManager dPM;
    private ComponentName adminName;

    private MdmCmdExecutor() {
        Application app = ChameleonApplication.getApplication();
        dPM = (DevicePolicyManager) app.getSystemService(Context.DEVICE_POLICY_SERVICE);
        adminName = new ComponentName(app, BslDeviceAdmin.class);
    }
    private static MdmCmdExecutor instance = new MdmCmdExecutor();
    public static MdmCmdExecutor getInstance() {
        return instance;
    }
    public boolean isActive() {
        return dPM.isAdminActive(adminName);
    }

    public ComponentName getAdminName() {
        return adminName;
    }

    public DevicePolicyManager getDpm() {
        return dPM;
    }

    public void active(Activity act) {
        if(!isActive()) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "点击 Activate按钮,激活MDM");
            act.startActivityForResult(intent, REQUEST_CODE_MDM);
        }
    }

    // 某些命令的值
    private static String alpha_digit = "alpha_digit";
    private static String alpha_digit_symbol = "alpha_digit_symbol";

    /**
     * @param cmd 需要执行的命令
     * @return true 表示执行成功,false失败
     * @throws Exception 有些操作可能失败
     */
    public boolean exec(ModelMdmCmd cmd) throws Exception {
        AppLog.i("开始执行 mdm type=" + cmd.cmdType.toString() + ",arg=" + cmd.cmdArg.toString());
        switch(cmd.cmdType) {
            case password_reset:
                dPM.resetPassword(cmd.cmdArg, 0);
                break;
            case password_min_len:
                int minLen = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordMinimumLength(adminName, minLen);
                break;
            case password_quality:
                if(alpha_digit_symbol.equals((cmd.cmdArg))) {
                    dPM.setPasswordQuality(adminName, DevicePolicyManager.PASSWORD_QUALITY_COMPLEX);
                } else if(alpha_digit.equals(cmd.cmdArg)) {
                    dPM.setPasswordQuality(adminName, DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC);
                }
                throw new RuntimeException("password_quality 参数不支持,args=" + cmd.cmdArg);
            case password_min_letter:
                int minLenLetter = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordMinimumLetters(adminName, minLenLetter);
                break;
            case password_min_letter_low:
                int minLenLow = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordMinimumLowerCase(adminName, minLenLow);
                break;
            case password_min_digit:
                int minDigitLen = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordMinimumNumeric(adminName, minDigitLen);
                break;
            case password_min_letter_cap:
                int minLenCap = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordMinimumUpperCase(adminName, minLenCap);
                break;
            case password_expire_time:
                int expireTimeout = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordExpirationTimeout(adminName, expireTimeout);
                break;
            case password_history_restrict:
                int newPwdMinLen = Integer.parseInt(cmd.cmdArg);
                dPM.setPasswordHistoryLength(adminName, newPwdMinLen);
                break;
            case password_max_fail:
                int maxFail = Integer.parseInt(cmd.cmdArg);
                dPM.setMaximumFailedPasswordsForWipe(adminName, maxFail);
                break;
            case lock_screen:
                dPM.lockNow();// 锁屏
                break;
            case timeout_screen:
                int screeTimeout = Integer.parseInt(cmd.cmdArg);
                dPM.setMaximumTimeToLock(adminName, screeTimeout);
                break;
            case camera_disable:
                boolean isDisableCamera = "true".equals(cmd.cmdArg);
                dPM.setCameraDisabled(adminName, isDisableCamera);
                break;
            case storage_crypt:
                boolean isEncrypt = "true".equals(cmd.cmdArg);
                dPM.setStorageEncryption(adminName, isEncrypt);
                break;
            case wipe_data:
                dPM.wipeData(0);
                break;
            default: // 该命令不存在
                AppLog.e("未找到的命令:" + cmd.cmdType.toString());
                return false;
        }
        return true;
    }
}
