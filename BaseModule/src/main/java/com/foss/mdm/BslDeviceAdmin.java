package com.foss.mdm;
import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import com.foss.AppLog;
/**
 * project:
 * author: wzq
 * date: 2014/7/28
 * description:
 */
public class BslDeviceAdmin extends DeviceAdminReceiver {
    //	implement onEnabled(), onDisabled(),
    @Override
    public void onReceive(Context context, Intent intent) {
        AppLog.d("onReceive");
        super.onReceive(context, intent);
    }

    public void onEnabled(Context context, Intent intent) {
        AppLog.d("onEnabled");
    }

    public void onDisabled(Context context, Intent intent) {
        AppLog.d("onDisabled");
    }
}
