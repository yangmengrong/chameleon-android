package com.foss.mdm;
/**
 * @author: wzq
 * @date: 14-7-31
 * description: 命令类型
 */
public enum MdmCmdType {
    password_reset("password_reset"), //ok
    password_min_len("password_min_len"),
    password_quality("password_quality"),
    password_min_letter("password_min_letter"),
    password_min_letter_low("password_min_letter_low"),
    password_min_digit("password_min_digit"),
    password_min_letter_cap("password_min_letter_cap"),
    password_expire_time("password_expire_time"),
    password_history_restrict("password_history_restrict"),
    password_max_fail("password_max_fail"),//ok 当魅族执行后3次失败
    lock_screen("lock_screen"),//ok
    timeout_screen("timeout_screen"), //ok
    storage_crypt("storage_crypt"),
    camera_disable("camera_disable"),//ok
    wipe_data("wipe_data");

    private MdmCmdType(String s) {
        str = s;
    }
    private String str;
    public String toString() {
        return str;
    }
}
