package com.foreveross.bsl.manager;
public interface CountChangeListener{
	public void onCountChange(int count, boolean displayBadge);
}