package com.foreveross.chameleon;

import com.foreveross.chameleon.manager.R;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;


public class MessageActivity extends FragmentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_activity_layout);
	}
}
